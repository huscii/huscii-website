+++
title = 'Huscii Home'
date = 2023-11-07T13:00:00-07:00
+++

Welcome to Huscii! We're dedicated to building an inclusive CS community on campus and around Puget Sound. Come develop together by working on collaborative projects, developing career planning skills, and chatting with like-minded people about CS! 

Check out our [upcoming events](events) or come chat with us in our [Discord server](https://discord.gg/NGX6TYPBQu) to learn more! 