+++
title = 'Coffee & Code'
date = 2024-01-03T11:00:00-07:00
categories = ['events']
showhero = 'true'
showsummary = 'true'
+++
**Wednesdays 12:00-1:20 @ Dancing Goats Coffee for winter quarter.**
# Coding assignments starting to test your mettle?
Come hang out with us and get some eyes on your code while you enjoy a delicious free coffee provided by Dancing Goats. Get some good advice, solve problems, or just have a chill hang out with other CS majors!

Dancing Goats Coffee is across 19th Ave from Mattress Factory, next to the "BREWERY BLOCKS" sign
![Map with red pin denoting location of Dancing Goats Coffee](goatmap.png)