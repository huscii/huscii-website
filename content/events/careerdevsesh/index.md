+++
title = 'Career Development Sessions'
date = 2024-01-03T10:00:00-07:00
categories = ['events']
showhero = 'true'
showsummary = 'true'
+++

**Mondays 12:30-1:20 in the Dawg House, first floor of Mattress Factory**
# Are you prepared for a career after graduation?
Get ready to join the workforce with Huscii! Every monday during Husky Hour we'll be meeting in the [Dawg House](https://www.tacoma.uw.edu/uwy/dhslo) to build coding skills, practice interviewing, and work on our resumes. Swing by to grind some leetcode with your classmates, or do a resume review to bump up your chances of landing that internship!

## Winter Quarter Agenda
**Jan 8**: Hangout/Network - get to know your peers and meet new attendees/members. Exchange LinkedIn's and contact info to expand your network.

**Jan 15**: Resume and Portfolio Peer Review - resumes and portfolios are probably updated post autumn quarter and winter break so we can check that

**Jan 22**: LeetCode Work - group focused, aimed to help members better explain their thought process

**Jan 29**: Mock Interviews - behavioral interview prep

**Feb  5**: LeetCode Work - individual focus meant to help members get the practice they need

**Feb 12**: Mock Interviews - technical interview prep

**Feb 19**: LeetCode work - individual focus, gives members a time and a place to grind out LeetCode

**Feb 26**: Resume and Portfolio Peer Review - members will probably have a lot to add at the end of the quarter so we will check in again

**March 4**: Finals prep and socialize - approaching the end of the quarter and everyone is stressed so this meeting is to finish up some assignments/projects and to hang out with peers