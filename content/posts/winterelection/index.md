+++
title = 'Winter Quarter 2024 Elections'
date = 2024-01-03T11:00:00-07:00
categories = ['posts', 'club operations', 'elections']
showhero = 'true'
showsummary = 'true'
+++
# Winter Quarter Elections
**Elections will be held online on Monday, January 15th!** check discord for more info.

**If you are interested in running** Check out the [proclamations](https://discord.com/channels/1034909312050532424/1157796246401060914) channel in the members only section of the discord and follow the link there to submit your campaign application. If you are not currently a member check out [welcome](https://discord.com/channels/1034909312050532424/1118299048609333258) channel for information on how to join!

In order to be eligible to run for office you must:
 - be a member in good standing with the club
 - have not held the office for which you are running for two or more consecutive quarters
 - not be graduating before the end of spring quarter 2024.

## Officer position descriptions:
**President**
 - Leader of the club. Sets agenda for the quarter and works with the officers and club members to host events and undertake projects.
 - Oversee events & club operations
 - Runs general membership meetings.
 - Permitted to name Committee members and Committee Chairs to organize and empower club members.

**Vice President**
 - Co-Leader of the club. Shares responsibility of the President.
 - All powers are shared with the President except in the case of either position taking unilateral action, in which the President may overrule the Vice President.

**Treasurer**
 - Manages club finances and keeps track of club resources.
 - Responsible for any interactions between the club and the University of Washington or any other party regarding income, earnings, and taxes.

**Secretary**
 - Manages event organization and scheduling.
 - Will be required to take the RSO Event Space Reservation training module offered by the University in order to carry out proper event management privileges.
   - In the event no other currently standing officers have taken this module, it is the Secretary’s responsibility to enlist one other officer (preferably the President) to take it as well.
 - General club management activities including working with other officers to ensure the smooth and efficient operation of club business.
